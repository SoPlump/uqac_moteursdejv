import { Point } from './point';
import { Rectangle } from '../src/components/rectangle';
import { maxDepth } from './utils';

interface QuadtreeItem<T> {
    rectangle: Rectangle;
    object: T;
}

export class Quadtree<T> extends Rectangle {
    private nwChild: Quadtree<T> | undefined;
    private neChild: Quadtree<T> | undefined;
    private seChild: Quadtree<T> | undefined;
    private swChild: Quadtree<T> | undefined;
    private children: Quadtree<T>[];
    private hasChildren: boolean;
    private items: QuadtreeItem<T>[];
    private depth: number;

    private constructor(x: number, y: number, width: number, height: number, depth: number) {
        super({
            xMin: x,
            xMax: x + width,
            yMin: y,
            yMax: y + height
        });
        this.children = [];
        this.items = [];
        this.hasChildren = false;
        this.depth = depth;
    }

    public clear(): void {
        this.nwChild = undefined;
        this.neChild = undefined;
        this.seChild = undefined;
        this.swChild = undefined;
        this.children = [];
        this.items = [];
        this.hasChildren = false;
    }

    public insert(rectangle: Rectangle, object: T): void {
        this.items.push({ rectangle, object });
        if (this.depth < maxDepth()) {
            if (!this.hasChildren) {
                this.appendChildren();
            }
            const child = this.children.find(c => c.fitsRectangle(rectangle));
            if (child !== undefined) {
                child.insert(rectangle, object);
            } else {
                // Test d'avec qui on s'intersecte
                this.children.forEach(child => {
                    if (child.intersectsWith(rectangle)) {
                        child.insert(rectangle, object);
                    }
                });
            }
        }
    }

    public remove(r: Rectangle): QuadtreeItem<T> | undefined {
        const child = this.children.find(c => c.fitsRectangle(r));
        if (child === undefined) {
            const index = this.items.findIndex(i => i.rectangle.equals(r));
            if (index === -1) {
                return undefined;
            }
            return this.items.splice(index, 1)[0];
        } else {
            return child.remove(r);
        }
    }

    public removeObject(o: T): QuadtreeItem<T> | undefined {
        const index = this.items.findIndex(i => i.object === o);
        if (index > -1) {
            return this.items.splice(index, 1)[0];
        }
        for (const c of this.children) {
            const r = c.removeObject(o);
            if (r !== undefined) {
                return r;
            }
        }
        return undefined;
    }

    public retrieve(point: Point): T | undefined {
        const rect = this.items.find(e => e.rectangle.pointInside(point));
        if (rect !== undefined) {
            return rect.object;
        }
        const child = this.children.find(c => c.pointInside(point));
        if (child !== undefined) {
            return child.retrieve(point);
        }
        return undefined;
    }
    /*
        public getQuadTree(object :T) : QuadtreeItem<T> | undefined {
            const item = this.items.find(i => i.object = object);
            if (item !== undefined) {
                return item;
            }
            const rec = this.children.find(r=>r.fitsRectangle(object.area()));
    
        } 
    */
    private appendChildren(): void {
        if (this.hasChildren) {
            return;
        }
        const childWidth = (this.xMax - this.xMin) / 2;
        const childHeight = (this.yMax - this.yMin) / 2;
        this.nwChild = new Quadtree(this.xMin, this.yMin, childWidth, childHeight, this.depth + 1);
        this.neChild = new Quadtree(this.xMin + childWidth, this.yMin, childWidth, childHeight, this.depth + 1);
        this.seChild = new Quadtree(this.xMin + childWidth, this.yMin + childHeight, childWidth, childHeight, this.depth + 1);
        this.swChild = new Quadtree(this.xMin, this.yMin + childHeight, childWidth, childHeight, this.depth + 1);
        this.children = [this.nwChild, this.neChild, this.seChild, this.swChild];
        this.hasChildren = true;

        // Ajouter les items aux enfants
    }

    public getNeighbouringObject(object: T, objects: Set<T>): void {
        if (this.depth === maxDepth()) {
            this.items.forEach(item => {
                objects.add(item.object);
            });
        } else {
            this.children.forEach(child => {
                const index = this.items.findIndex(i => i.object === object);
                if (index > -1) {
                    child.getNeighbouringObject(object, objects);
                }
            });
        }
    }

    public static createQuadtree<T>(width: number, height: number): Quadtree<T> {
        return new Quadtree<T>(105, 0, width, height, 0);
    }
}
