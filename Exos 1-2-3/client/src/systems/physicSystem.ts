import { ColliderComponent } from "../components/colliderComponent";
import { Quadtree } from "../quadtree";
import { Scene } from "../scene";
import { ISystem } from "./system";
import { IEntity } from "../entity";

// # Classe *PhysicSystem*
// Représente le système permettant de détecter les collisions
export class PhysicSystem implements ISystem {
  // Méthode *iterate*
  // Appelée à chaque tour de la boucle de jeu
  public iterate(dT: number) {

    let subdivisions = Quadtree.createQuadtree<ColliderComponent>(148, 108);
    const colliders: ColliderComponent[] = [];

    for (const e of Scene.current.entities()) {
      for (const comp of e.components) {
        if (comp instanceof ColliderComponent && comp.enabled) {
          colliders.push(comp);
          subdivisions.insert(comp.area, comp);
        }
      }
    }

    const collisions: Array<[ColliderComponent, ColliderComponent]> = [];

    colliders.forEach(collider => {
      if (collider.enabled && collider.owner.active) {
        let possibleColliders = new Set<ColliderComponent>();
        subdivisions.getNeighbouringObject(collider, possibleColliders);
        possibleColliders.delete(collider);

        possibleColliders.forEach(possibleCollider => {
          if (possibleCollider.enabled && possibleCollider.owner.active &&
            (((collider.flag & possibleCollider.mask) !== 0) || ((possibleCollider.flag & collider.mask) !== 0))
            && (collider.area.intersectsWith(possibleCollider.area))) {
            collisions.push([collider, possibleCollider]);
          }
        });
      }
    })



    // for (let i = 0; i < colliders.length; i++) {
    //   const c1 = colliders[i];
    //   if (!c1.enabled || !c1.owner.active) {
    //     continue;
    //   }

    //   for (let j = i + 1; j < colliders.length; j++) {
    //     const c2 = colliders[j];
    //     if (!c2.enabled || !c2.owner.active) {
    //       continue;
    //     }

    //     if (c1.area.intersectsWith(c2.area)) {
    //       if (((c1.flag & c2.mask) !== 0) || ((c2.flag & c1.mask) !== 0)) {
    //         collisions.push([c1, c2]);
    //       }
    //     }
    //   }
    // }

    for (const [c1, c2] of collisions) {
      if (c1.handler) {
        c1.handler.onCollision(c2);
      }
    }
  }
}
