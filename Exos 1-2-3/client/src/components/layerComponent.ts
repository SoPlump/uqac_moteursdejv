import { IEntity } from "../entity";
import { IDisplayComponent } from "../systems/displaySystem";
import { Component } from "./component";
import { SpriteComponent } from "./spriteComponent";
import * as GraphicsAPI from "../graphicsAPI";
import { TextureComponent } from "./textureComponent";

let GL: WebGLRenderingContext;

// # Classe *LayerComponent*
// Ce composant représente un ensemble de sprites qui
// doivent normalement être considérées comme étant sur un
// même plan.
export class LayerComponent extends Component<object> implements IDisplayComponent {
  private vertices!: Float32Array;
  private indices!: Uint16Array;
  private indexBuffer!: WebGLBuffer;
  private vertexBuffer!: WebGLBuffer;

  // ## Méthode *display*
  // La méthode *display* est appelée une fois par itération
  // de la boucle de jeu.
  public display(dT: number) {
    GL = GraphicsAPI.context;

    const layerSprites = this.listSprites();
    if (layerSprites.length === 0) {
      return;
    }
    const spriteSheet = layerSprites[0].spriteSheet;

    this.vertices = new Float32Array(layerSprites.length * 4 * TextureComponent.vertexSize);
    this.indices = new Uint16Array(layerSprites.length * 6);

    // Concat vertices and indices of all sprites of the layer

    for (let i = 0; i < layerSprites.length; ++i) {
      this.vertices.set(layerSprites[i].vertices, 4 * TextureComponent.vertexSize * i);
      this.indices.set(new Uint16Array([4 * i, 1 + 4 * i, 2 + 4 * i, 2 + 4 * i, 3 + 4 * i, 4 * i]), i * 6);
    }

    // Vertices

    this.vertexBuffer = GL.createBuffer()!;
    GL.bindBuffer(GL.ARRAY_BUFFER, this.vertexBuffer);
    GL.bufferData(GL.ARRAY_BUFFER, this.vertices, GL.DYNAMIC_DRAW);
    
    // Indices

    this.indexBuffer = GL.createBuffer()!;
    GL.bindBuffer(GL.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
    GL.bufferData(GL.ELEMENT_ARRAY_BUFFER, this.indices, GL.DYNAMIC_DRAW);

    spriteSheet.bind();
    GL.drawElements(GL.TRIANGLES, 6 * layerSprites.length, GL.UNSIGNED_SHORT, 0);
    spriteSheet.unbind();
  }

  // ## Fonction *listSprites*
  // Cette fonction retourne une liste comportant l'ensemble
  // des sprites de l'objet courant et de ses enfants.
  private listSprites() {
    const sprites: SpriteComponent[] = [];

    const queue: IEntity[] = [this.owner];
    while (queue.length > 0) {
      const node = queue.shift() as IEntity;
      for (const child of node.children) {
        if (child.active) {
          queue.push(child);
        }
      }

      for (const comp of node.components) {
        if (comp instanceof SpriteComponent && comp.enabled) {
          sprites.push(comp);
        }
      }
    }

    return sprites;
  }
}
